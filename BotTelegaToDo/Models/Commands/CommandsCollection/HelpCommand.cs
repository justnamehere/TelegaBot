using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using BotTelegaToDo.Models;
using BotTelegaToDo.Services.Interfaces;
using System.Collections.Generic;

namespace BotTelegaToDo.Models.Commnands.CommandsCollection{
    public class HelpCommand : Command
    {        
        public override string Name => @"/help";

        public override bool Contains(Message message)
        {
           if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task ExecuteAsync(Message message, TelegramBotClient client,IUsageService service)
        {
            await service.Help(message,client);
        }
    }
}