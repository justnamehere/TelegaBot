using System;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace BotTelegaToDo.Services.Interfaces{
    public interface IUsageService : IDisposable
    {
        //string callbackString{get;set;}
        Task CreateUser(Message message, TelegramBotClient client);
        Task AddTask(Message message, TelegramBotClient client);
        Task EditTask(Message message, TelegramBotClient client);
        Task ShowTasks(Message message, TelegramBotClient client);
        Task SwapTasks(Message message, TelegramBotClient client);
        Task DeleteTask(Message message, TelegramBotClient client);
        Task Help(Message message, TelegramBotClient client);
    } 
}