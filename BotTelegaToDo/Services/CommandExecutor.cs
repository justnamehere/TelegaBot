using System.Threading.Tasks;
using BotTelegaToDo.Models;
using BotTelegaToDo.Services.Interfaces;
using Telegram.Bot.Types;

namespace BotTelegaToDo.Services{
    public static class CommandExecutor{
        public static async Task<bool> Execute(Update update, IUsageService service){
            var result = false;
            Bot bot = await Bot.GetInstance();
            var message = update.Message;            

            foreach (var command in bot.commandsList)
            {
                if (command.Contains(message))
                {
                    result = true;
                    await command.ExecuteAsync(message, bot.botClient, service);
                    break;
                }
            }

            return result;            
        }             
    }
}
