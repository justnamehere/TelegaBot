using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BotTelegaToDo.Models;
using BotTelegaToDo.Models.Db;
using BotTelegaToDo.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace BotTelegaToDo.Services
{
    class UsageService : IUsageService
    {
        private readonly ApplicationDbContext db;
        private string callbackString = string.Empty;

        public UsageService(ApplicationDbContext db)
        {
            this.db = db;
        }

        public async Task CreateUser(Message message, TelegramBotClient client)
        {
            var user = db.Users.FirstOrDefault(x => x.Id == message.From.Id);
            if (user != null)
            {
                if (user.Username != message.From.Username)
                {
                    user.Username = message.From.Username;
                    db.Users.Update(user);
                    await db.SaveChangesAsync();
                }
                callbackString = string.Format("{0}, familiarize with it /help", message.From.Username);

                await client.SendTextMessageAsync(message.Chat.Id, callbackString,
                replyToMessageId: message.MessageId);
            }
            else
            {
                user = new User_(message.From.Id, message.From.Username);
                await db.Users.AddAsync(user);
                await db.SaveChangesAsync();

                callbackString = string.Format("{0}, you have started working with the bot", user.Username);
                await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
            }
        }

        public async Task AddTask(Message message, TelegramBotClient client)
        {
            var user = db.Users.Include(x => x.Tasks).FirstOrDefault(x => x.Id == message.From.Id);
            if (user == null)
            {
                callbackString = string.Format("Please start command /start to begin working with the bot", user.Username);
                await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
            }
            else
            {
                var parameters = GetParameters(message);
                if (parameters.Length != 2)
                {
                    callbackString = "Usage: /addtask <name> <date>";
                    await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                }
                else
                {
                    var taskInBase = user.Tasks.FirstOrDefault(x => x.Name == parameters[0]);
                    if (taskInBase != null)
                    {
                        callbackString = string.Format("Task with the name {0} is already consisit", parameters[0]);
                        await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                    }
                    else
                    {
                        DateTime date;
                        if (StringDateParser(parameters[1], out date) && date > DateTime.Now)
                        {
                            Task_ task = new Task_(parameters[0], date);
                            user.Tasks.Add(task);
                            db.Users.Update(user);
                            await db.SaveChangesAsync();

                            callbackString = string.Format("Task {0} was added on date {1}", task.Name, task.FinishedDate.ToString());
                            await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                        }
                        else
                        {
                            callbackString = "<date>: dd.MM.yyyy";
                            await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                        }
                    }
                }
            }
        }

        public async Task EditTask(Message message, TelegramBotClient client)
        {
            var user = db.Users.Include(x => x.Tasks).FirstOrDefault(x => x.Id == message.From.Id);
            if (user == null)
            {
                callbackString = string.Format("Please start command /start to begin working with the bot", user.Username);
                await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
            }
            else
            {
                var parameters = GetParameters(message);
                if (parameters.Length != 2)
                {
                    callbackString = "Usage: /edit <name> <new date>";
                    await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                }
                else
                {
                    var taskInBase = user.Tasks.FirstOrDefault(x => x.Name == parameters[0]);
                    if (taskInBase == null)
                    {
                        callbackString = string.Format("Task with the name {0} was not found", parameters[0]);
                        await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                    }
                    else
                    {
                        DateTime date;
                        if (StringDateParser(parameters[1], out date) && date > DateTime.Now)
                        {
                            taskInBase.FinishedDate = date;                            
                            db.Tasks.Update(taskInBase);
                            await db.SaveChangesAsync();

                            callbackString = string.Format("Task {0} was set on date {1}", taskInBase.Name, taskInBase.FinishedDate.ToString());
                            await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                        }
                        else
                        {
                            callbackString = "<date>: dd.MM.yyyy";
                            await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                        }
                    }
                }
            }
        }

        public async Task ShowTasks(Message message, TelegramBotClient client)
        {
            var user = await db.Users.Include(x => x.Tasks).FirstOrDefaultAsync(x => x.Id == message.From.Id);
            if (user == null)
            {
                callbackString = string.Format("Please start command /start to begin working with the bot", user.Username);
                await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
            }
            else
            {
                foreach (var item in user.Tasks)
                {
                    callbackString += string.Format("{0} - {1} \n", item.Name, item.FinishedDate.Date.ToString());
                }
            }

            await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
        }

        public async Task SwapTasks(Message message, TelegramBotClient client)
        {
            var user = await db.Users.Include(x => x.Tasks).FirstOrDefaultAsync(x => x.Id == message.From.Id);
            if (user == null)
            {
                callbackString = string.Format("Please start command /start to begin working with the bot", user.Username);
                await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
            }
            else
            {
                var parameters = GetParameters(message);
                if (parameters.Length != 2)
                {
                    callbackString = "Usage: /swap <task name1> <task name2>";
                    await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                }
                else
                {
                    var taskFirst = user.Tasks.FirstOrDefault(x => x.Name == parameters[0]);
                    var taskSecond = user.Tasks.FirstOrDefault(x => x.Name == parameters[1]);
                    if (taskFirst != null && taskSecond != null)
                    {
                        var temp = taskFirst.FinishedDate;
                        taskFirst.FinishedDate = taskSecond.FinishedDate;
                        taskSecond.FinishedDate = temp;

                        db.Tasks.UpdateRange(taskFirst, taskSecond);
                        await db.SaveChangesAsync();

                        callbackString = "Tasks were swaped";
                        await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                    }
                    else
                    {
                        callbackString = "Incorrect names";
                        await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                    }
                }
            }
        }

        public async Task DeleteTask(Message message, TelegramBotClient client)
        {
            var user = await db.Users.Include(x => x.Tasks).FirstOrDefaultAsync(x => x.Id == message.From.Id);
            if (user == null)
            {
                callbackString = string.Format("Please start command /start to begin working with the bot", user.Username);
                await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
            }
            else
            {
                var parameters = GetParameters(message);
                if (parameters.Length != 1)
                {
                    callbackString = "Usage: /delete <task name>";
                    await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                }
                else
                {
                    var task = user.Tasks.FirstOrDefault(x => x.Name == parameters[0]);
                    if (task != null)
                    {
                        db.Tasks.Remove(task);
                        await db.SaveChangesAsync();

                        callbackString = "Task was deleted";
                        await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                    }
                    else
                    {
                        callbackString = "Incorrect name";
                        await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
                    }
                }
            }
        }

        public async Task Help(Message message, TelegramBotClient client)
        {
            var user = await db.Users.Include(x => x.Tasks).FirstOrDefaultAsync(x => x.Id == message.From.Id);
            if (user == null)
            {
                callbackString = string.Format("Please start command /start to begin working with the bot", user.Username);
                await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
            }else{
                callbackString += "Here is the list of commands: \n";
                callbackString += " *Usage: /add <name> <date> \n";
                callbackString += " *Usage: /edit <name> <new date> \n";
                callbackString += " *Usage: /swap <task name1> <task name2> \n";
                callbackString += " *Usage: /delete <name> \n";
                callbackString += " *Usage: /show \n";
                callbackString += " *Usage: /help";

                await client.SendTextMessageAsync(message.Chat.Id, callbackString, replyToMessageId: message.MessageId);
            }
        }

        private string[] GetParameters(Message message)
        {
            string text = message.Text;
            string raw = text.Substring(message.Entities[0].Length);
            string[] parameters = raw.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            return parameters;
        }

        private bool StringDateParser(string stringDate, out DateTime date)
        {
            var result = DateTime.TryParseExact(stringDate, "dd.MM.yyyy",
             System.Globalization.CultureInfo.InvariantCulture,
             System.Globalization.DateTimeStyles.None, out date);

            return result;
        }

        public void Dispose()
        {
            db.Dispose();
        }       
    }
}